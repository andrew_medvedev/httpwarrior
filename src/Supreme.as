package {
	import flash.display.Sprite;
	import flash.events.Event;
	import http_warrior.HttpWarrior;
	import http_warrior.HttpWarriorHolder;
	import http_warrior.HttpWarriorResponse;
	import utils.NextFrameCallback;
	
	/**
	 * ...
	 * @author nh
	 */
	public class Supreme extends Sprite {
		
		private var http:HttpWarriorHolder = null;
		private var nfc:NextFrameCallback = null;
		
		public function Supreme():void {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			http = HttpWarrior.Holder;
			nfc = NextFrameCallback.Instance;
			
			/*
			 *  Usage of Http Warrior:
			 * 
			 * */
			
			/*
			 *  make instance
			 *  @param _interval - interval(ms) between reqs
			 * 
			 * */
			var httpw:HttpWarrior = HttpWarrior.getInstance(100);
			
			var req_host:String = "http://localhost/";
			
			/*
			 * doing GET req
			 * @param _urlBuilderLambda - a lambda which should return req string
			 * @param _method - URL method
			 * @param _data - req body
			 * @param _contentType - MIME-type
			 * 
			 * */
			var resp:HttpWarriorResponse = httpw.doReq(function():String {
				return req_host;
			}, "get");
			
			/*
			 * on req complete callback
			 * 
			 * */
			resp.Listeners.onComplete(function():void {
				trace("onComplete()");
				trace("data = " + resp.Data.readUTFBytes(resp.Data.length));
			});
			
			/*
			 * on req error callback
			 * 
			 * */
			resp.Listeners.onError(function():void {
				trace("onError()");
			});
			
			stage.addEventListener(Event.ENTER_FRAME, ef);
		}
		private function ef(e:Event):void {
			http.step();
			nfc.step();
		}
	}
}