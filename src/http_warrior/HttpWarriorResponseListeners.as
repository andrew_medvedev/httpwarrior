package http_warrior {
	import utils.Listeners;
	
	/**
	 * ...
	 * @author Naughty Horse
	 */
	public final class HttpWarriorResponseListeners extends Listeners {
		private static const BEGIN_EVENT:String = "s";
		private static const PROGRESS_EVENT:String = "h";
		private static const ERROR_EVENT:String = "i";
		private static const COMPLETE_EVENT:String = "t";
		
		public function HttpWarriorResponseListeners() {
			super();
		}
		public function clearListeners():void {
			this.clear();
		}
		public function onBegin(_callback:Function):void {
			this.addListener(BEGIN_EVENT, _callback);
		}
		public function onProgress(_callback:Function):void {
			this.addListener(PROGRESS_EVENT, _callback);
		}
		public function onError(_callback:Function):void {
			this.addListener(ERROR_EVENT, _callback);
		}
		public function onComplete(_callback:Function):void {
			this.addListener(COMPLETE_EVENT, _callback);
		}
		public function removeOnBegin(_callback:Function):void {
			this.removeListener(BEGIN_EVENT, _callback);
		}
		public function removeOnProgress(_callback:Function):void {
			this.removeListener(PROGRESS_EVENT, _callback);
		}
		public function removeOnError(_callback:Function):void {
			this.removeListener(ERROR_EVENT, _callback);
		}
		public function removeOnComplete(_callback:Function):void {
			this.removeListener(COMPLETE_EVENT, _callback);
		}
		internal function dispatchOnBegin():void {
			this.dispatchEvent(BEGIN_EVENT);
		}
		internal function dispatchOnProgress():void {
			this.dispatchEvent(PROGRESS_EVENT);
		}
		internal function dispatchOnError():void {
			this.dispatchEvent(ERROR_EVENT);
		}
		internal function dispatchOnComplete():void {
			this.dispatchEvent(COMPLETE_EVENT);
		}
	}
}