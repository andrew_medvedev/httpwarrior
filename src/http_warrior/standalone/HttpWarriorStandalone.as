package http_warrior.standalone {
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author Naughty Horse
	 */
	public final class HttpWarriorStandalone {
		public static const STATE_UNDEFINED:int = 0;
		public static const STATE_PROGRESS:int = 1;
		public static const STATE_FAILED:int = 2;
		public static const STATE_DONE:int = 3;
		
		private var stream:URLStream = null;
		
		private var url:String = null;
		private var method:String = null;
		
		private var data:Object = null;
		private var contentType:String = null;
		
		private var state:int = STATE_UNDEFINED;
		
		private var bytesLoaded:int = 0;
		private var bytesTotal:int = 0;
		
		private var httpCode:int = 0;
		
		private var dataOut:ByteArray = null;
		
		public var onBegin:Function = null;
		public var onProgress:Function = null;
		public var onError:Function = null;
		public var onComplete:Function = null;
		
		public function HttpWarriorStandalone() {
			
		}
		public function doReq(_url:String, _method:String, _data:Object = null, _contentType:String = null):void {
			url = _url;
			method = _method;
			data = _data;
			contentType = _contentType;
			
			stream = new URLStream();
			var _req:URLRequest = new URLRequest();
			_req.url = url;
			_req.data = data;
			if (contentType)
				_req.contentType = contentType;
			_req.method = method.toUpperCase();
			
			stream.addEventListener(Event.OPEN, beginH);
			stream.addEventListener(IOErrorEvent.IO_ERROR, errorH);
			stream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorH);
			stream.addEventListener(ProgressEvent.PROGRESS, progressH);
			stream.addEventListener(HTTPStatusEvent.HTTP_STATUS, statusH);
			stream.addEventListener(Event.COMPLETE, completeH);
			
			stream.load(_req);
		}
		private function dropStream():void {
			stream.removeEventListener(Event.OPEN, beginH);
			stream.removeEventListener(IOErrorEvent.IO_ERROR, errorH);
			stream.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, errorH);
			stream.removeEventListener(ProgressEvent.PROGRESS, progressH);
			stream.removeEventListener(HTTPStatusEvent.HTTP_STATUS, statusH);
			stream.removeEventListener(Event.COMPLETE, completeH);
			if (stream.connected)
				stream.close();
			stream = null;
		}
		private function beginH(e:Event):void {
			state = STATE_PROGRESS;
			dataOut = new ByteArray();
			if (onBegin != null)
				onBegin();
		}
		private function errorH(e:Object):void {
			state = STATE_FAILED;
			if (onError != null)
				onError();
			
			dropStream();
		}
		private function progressH(e:ProgressEvent):void {
			bytesLoaded += stream.bytesAvailable;
			stream.readBytes(dataOut);
			bytesTotal = e.bytesTotal;
			if (onProgress != null)
				onProgress();
		}
		private function statusH(e:HTTPStatusEvent):void {
			httpCode = e.status;
		}
		private function completeH(e:Event):void {
			state = STATE_DONE;
			if (onComplete != null)
				onComplete();
			
			dropStream();
		}
		public function get State():int {
			return state;
		}
		public function get BytesLoaded():int {
			return bytesLoaded;
		}
		public function get BytesTotal():int {
			return bytesTotal;
		}
		public function get HttpCode():int {
			return httpCode;
		}
		public function get Data():ByteArray {
			return dataOut;
		}
	}
}