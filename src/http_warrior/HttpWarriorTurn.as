package http_warrior {
	/**
	 * ...
	 * @author Naughty Horse
	 */
	internal final class HttpWarriorTurn {
		
		public var urlBuilderLambda:Function = null;
		public var method:String = null;
		
		public var data:Object = null;
		public var contentType:String = null;
		
		public var response:HttpWarriorResponse = null;
		
		public function HttpWarriorTurn(_urlBuilderLambda:Function, _method:String, _data:Object, _contentType:String, _response:HttpWarriorResponse) {
			urlBuilderLambda = _urlBuilderLambda;
			method = _method;
			data = _data;
			contentType = _contentType;
			response = _response;
		}
	}
}