package http_warrior {
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author Naughty Horse
	 */
	public final class HttpWarriorResponse {
		public static const STATE_UNDEFINED:int = 0;
		public static const STATE_PROGRESS:int = 1;
		public static const STATE_FAILED:int = 2;
		public static const STATE_DONE:int = 3;
		
		private var state:int = STATE_UNDEFINED;
		
		private var bytesLoaded:int = 0;
		private var bytesTotal:int = 0;
		
		private var httpCode:int = 0;
		
		private var data:ByteArray = null;
		
		internal var listeners:HttpWarriorResponseListeners = null;
		
		private var warrior:HttpWarrior = null;
		
		public function HttpWarriorResponse(_warrior:HttpWarrior) {
			warrior = _warrior;
			listeners = new HttpWarriorResponseListeners();
		}
		public function stop():void {
			warrior.stopTurn(this);
		}
		public function get Listeners():HttpWarriorResponseListeners {
			return listeners;
		}
		public function get State():int {
			return state;
		}
		internal function setState(value:int):void {
			state = value;
		}
		public function get BytesLoaded():int {
			return bytesLoaded;
		}
		internal function setBytesLoaded(value:int):void {
			bytesLoaded = value;
		}
		public function get BytesTotal():int {
			return bytesTotal;
		}
		internal function setBytesTotal(value:int):void {
			bytesTotal = value;
		}
		public function get HttpCode():int {
			return httpCode;
		}
		internal function setHttpCode(value:int):void {
			httpCode = value;
		}
		public function get Data():ByteArray {
			return data;
		}
		internal function setData(value:ByteArray):void {
			data = value;
		}
	}
}