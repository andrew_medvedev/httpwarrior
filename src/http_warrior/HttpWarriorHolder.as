package http_warrior {
	/**
	 * ...
	 * @author Naughty Horse
	 */
	public final class HttpWarriorHolder {
		
		public var warriors:Vector.<HttpWarrior> = null;
		
		private var _i:int = 0;
		private var _i2:int = 0;
		
		public function HttpWarriorHolder() {
			warriors = new Vector.<HttpWarrior>();
		}
		public function step():void {
			_i2 = warriors.length;
			if (_i2 > 0)
				for (_i = 0; _i < _i2; _i++ )
					warriors[_i].step();
		}
	}
}