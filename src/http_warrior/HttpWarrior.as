package http_warrior {
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author Naughty Horse
	 */
	public final class HttpWarrior {
		
		/*
		 *  Static
		 * */
		
		private static var holder:HttpWarriorHolder = null;
		
		public static function get Holder():HttpWarriorHolder {
			return holder ? holder : holder = new HttpWarriorHolder();
		}
		
		public static function getInstance(_interval:int):HttpWarrior {
			var _instance:HttpWarrior = new HttpWarrior(new Singleton, _interval);
			Holder.warriors[Holder.warriors.length] = _instance;
			
			return _instance;
		}
		
		/*
		 *  Instance
		 * */
		
		private var queue:Vector.<HttpWarriorTurn> = null;
		private var queueRun:Boolean = false;
		private var processTurnFlag:Boolean = false;
		
		private var tempTurn:HttpWarriorTurn = null;
		
		private var stream:URLStream = null;
		
		public var interval:int = 0;
		
		private var elapsed1:Number = 0;
		private var elapsed2:Number = 0;
		private var elapsed3:Number = 0;
		
		public function HttpWarrior(_protect:Singleton, _interval:int) {
			queue = new Vector.<HttpWarriorTurn>();
		}
		public function doReq(_urlBuilderLambda:Function, _method:String, _data:Object = null, _contentType:String = null):HttpWarriorResponse {
			var _resp:HttpWarriorResponse = new HttpWarriorResponse(this);
			queue[queue.length] = new HttpWarriorTurn(_urlBuilderLambda, _method, _data, _contentType, _resp);
			checkQueueRun();
			
			return _resp;
		}
		private function checkQueueRun():void {
			if (!queueRun) {
				queueRun = true;
				runQueue();
			}
		}
		private function runQueue():void {
			if (queue.length > 0) {
				tempTurn = queue.shift();
				processTurnFlag = true;
			} else {
				tempTurn = null;
				queueRun = false;
			}
		}
		private function processTurn():void {
			stream = new URLStream();
			var _req:URLRequest = new URLRequest();
			_req.url = tempTurn.urlBuilderLambda();
			_req.data = tempTurn.data;
			if (tempTurn.contentType)
				_req.contentType = tempTurn.contentType;
			_req.method = tempTurn.method.toUpperCase();
			
			stream.addEventListener(Event.OPEN, beginH);
			stream.addEventListener(IOErrorEvent.IO_ERROR, errorH);
			stream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorH);
			stream.addEventListener(ProgressEvent.PROGRESS, progressH);
			stream.addEventListener(HTTPStatusEvent.HTTP_STATUS, statusH);
			stream.addEventListener(Event.COMPLETE, completeH);
			
			stream.load(_req);
		}
		private function dropStream():void {
			stream.removeEventListener(Event.OPEN, beginH);
			stream.removeEventListener(IOErrorEvent.IO_ERROR, errorH);
			stream.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, errorH);
			stream.removeEventListener(ProgressEvent.PROGRESS, progressH);
			stream.removeEventListener(HTTPStatusEvent.HTTP_STATUS, statusH);
			stream.removeEventListener(Event.COMPLETE, completeH);
			if (stream.connected)
				stream.close();
			stream = null;
			
			elapsed3 = interval;
			runQueue();
		}
		private function beginH(e:Event):void {
			tempTurn.response.setState(HttpWarriorResponse.STATE_PROGRESS);
			tempTurn.response.setData(new ByteArray());
			tempTurn.response.listeners.dispatchOnBegin();
		}
		private function errorH(e:Object):void {
			tempTurn.response.setState(HttpWarriorResponse.STATE_FAILED);
			tempTurn.response.listeners.dispatchOnError();
			
			dropStream();
		}
		private function progressH(e:ProgressEvent):void {
			tempTurn.response.setBytesLoaded(tempTurn.response.BytesLoaded + stream.bytesAvailable);
			stream.readBytes(tempTurn.response.Data);
			tempTurn.response.setBytesTotal(e.bytesTotal);
			tempTurn.response.listeners.dispatchOnProgress();
		}
		private function statusH(e:HTTPStatusEvent):void {
			tempTurn.response.setHttpCode(e.status);
		}
		private function completeH(e:Event):void {
			tempTurn.response.setState(HttpWarriorResponse.STATE_DONE);
			tempTurn.response.listeners.dispatchOnComplete();
			
			dropStream();
		}
		internal function stopTurn(_response:HttpWarriorResponse):void {
			if (queueRun)
				if (tempTurn.response == _response)
					dropStream();
				else
					for (var i:int = 0; i < queue.length; i++ )
						if (queue[i].response == _response) {
							queue.splice(i, 1);
							return;
						}
		}
		internal function step():void {
			elapsed2 = new Date().getTime();
			elapsed3 -= (elapsed2 - elapsed1);
			elapsed1 = elapsed2;
			if (elapsed3 < 0)
				elapsed3 = 0;
			if (processTurnFlag && elapsed3 == 0) {
				processTurnFlag = false;
				processTurn();
			}
		}
	}
}
class Singleton { }