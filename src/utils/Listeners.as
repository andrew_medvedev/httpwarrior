package utils {
	/**
	 * ...
	 * @author Naughty Horse
	 */
	public class Listeners {
		
		private var listeners:Object = null;
		
		private var _vec:Vector.<Function> = null;
		
		public function Listeners() {
			listeners = { };
		}
		protected function addListener(_event:String, _callback:Function):void {
			if (!listeners[_event])
				listeners[_event] = new Vector.<Function>();
			listeners[_event].push(_callback);
		}
		protected function removeListener(_event:String, _callback:Function):void {
			NextFrameCallback.Instance.addCallback(function():void {
				_vec = listeners[_event] as Vector.<Function>;
				if (_vec)
					for (var i:int = 0; i < _vec.length; i++ )
						if (_vec[i] == _callback)
							_vec.splice(i--, 1);
				_vec = null;
			});
		}
		protected function removeAllListeners(_event:String):void {
			delete listeners[_event];
		}
		protected function clear():void {
			listeners = { };
		}
		protected function dispatchEvent(_event:String):void {
			_vec = listeners[_event] as Vector.<Function>;
			if (_vec)
				for (var i:int = 0; i < _vec.length; i++ )
					_vec[i]();
			_vec = null;
		}
	}
}