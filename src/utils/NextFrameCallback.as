package utils {
	/**
	 * ...
	 * @author Naughty Horse
	 */
	public final class NextFrameCallback {
		
		/*
		 *  Static
		 * */
		
		private static var instance:NextFrameCallback = null;
		
		public static function get Instance():NextFrameCallback {
			return instance ? instance : instance = new NextFrameCallback(new Singleton);
		}
		
		/*
		 *  Instance
		 * */
		
		private var callbacks:Vector.<Function> = null;
		
		private var _i:int = 0;
		private var _i2:int = 0;
		
		public function NextFrameCallback(_protection:Singleton) {
			callbacks = new Vector.<Function>();
		}
		public function addCallback(_callback:Function):void {
			callbacks[callbacks.length] = _callback;
		}
		public function step():void {
			_i2 = callbacks.length;
			if (_i2 > 0) {
				for (_i = 0; _i < _i2; _i++ )
					callbacks[_i]();
				callbacks.splice(0, _i2);
			}
		}
	}
}
class Singleton{}